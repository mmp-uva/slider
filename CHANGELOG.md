# CHANGELOG

## 0.1.5

* Resolves namespace issues

## 0.1.4

* Support the key column to be defined with a quosure.

## 0.1.3

* Fix usage of map in slide_window
